import { Injectable } from '@angular/core';

import { Yeast } from '../model/yeast';
import { Taxonomy } from '../model/taxonomy';

import { Products } from '../model/products';
import { Fermentation } from '../model/fermentation';

/**
 *we save different component methods
 *
 * @export
 * @class YeastService
 */
@Injectable({
  providedIn: 'root'
})
export class YeastService {

  constructor() { }

  /**
   *method crate a array of fermentation. 
   *Related with products.component.ts
   * @returns {Fermentation[]}
   * @memberof YeastService
   */
  createFermentationSelect2(): Fermentation[] {
    let arrayFermentation: Fermentation[] = [];
    let productTypeAux: string[] = ["Alcoholic Fermentation", "Lactic Acid Fermentation", "Propionic Acid Fermentation", "Butyric Acid — Butanol Fermentation"];
    let tableFermentation: Fermentation;

    for (let i: number = 0; i < productTypeAux.length; i++) {
      tableFermentation = new Fermentation(i, productTypeAux[i]);
      arrayFermentation.push(tableFermentation);
    }
    return arrayFermentation;
  }


  /**
   *methos that generate yeast random .
   * Related with yeast-management.component.ts
   * @returns {Yeast[]}
   * @memberof YeastService
   */
  generateYeastsRandom(): Yeast[] {
    let yeasts: Yeast[] = [];
    let randomId: string;
    let today2: string;
    today2 = new Date().toISOString().split('T')[0];
    let randomTemp: number;

    let yeast: Yeast;


    for (let i = 0; i < 369; i++) {
      let x = Math.random() * 501;
      randomTemp = Number(x.toFixed(2));

      if (i % 5 == 0) {
        randomId = "Amapola";
      } else {
        randomId = "ABCGTNT" + i;
      }

      yeast = new Yeast(randomId, new Taxonomy(i, "Procariota", "Fungi", "Phylum" + i, "Class" + i,
        "Order" + 1, "Family" + i, "Genus" + i, "Specie" + randomId + i, "A" + i + randomId),
        today2, randomTemp);

      yeasts.push(yeast);
      //let today: Date = new Date();
      //today.getDate() + "/" +(today.getMonth() + 1) + "/" + today.getFullYear()
    }
    return yeasts;
  }
}



