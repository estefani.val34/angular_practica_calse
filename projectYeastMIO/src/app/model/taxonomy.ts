/**
 *class taxonomy. Related with component yeast. 
 *
 * @export
 * @class Taxonomy
 */
export class Taxonomy{
  
    private _id: number;
    private _domain: string;
    private _kingdom: string;
    private _phylum: string;
    private _classe: string;
    private _order: string; 
    private _family: string;
    private _genus: string;
    private _species: string;
    private _variety: string;

	/**
     *Creates an instance of Taxonomy.
     * @param {number} [id]
     * @param {string} [domain]
     * @param {string} [kingdom]
     * @param {string} [phylum]
     * @param {string} [classe]
     * @param {string} [order]
     * @param {string} [family]
     * @param {string} [genus]
     * @param {string} [species]
     * @param {string} [variety]
     * @memberof Taxonomy
     */
    constructor(id?:number, domain?: string, kingdom?: string, phylum?: string, classe?: string, order?: string, family?: string, genus?: string, species?: string, variety?: string) {
        this._id=id;
        this._domain = domain;
		this._kingdom = kingdom;
		this._phylum = phylum;
		this._classe = classe;
		this._order = order;
		this._family = family;
		this._genus = genus;
		this._species = species;
		this._variety = variety;
	}


    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Getter domain
     * @return {string}
     */
	public get domain(): string {
		return this._domain;
	}

    /**
     * Getter kingdom
     * @return {string}
     */
	public get kingdom(): string {
		return this._kingdom;
	}

    /**
     * Getter phylum
     * @return {string}
     */
	public get phylum(): string {
		return this._phylum;
	}

    /**
     * Getter classe
     * @return {string}
     */
	public get classe(): string {
		return this._classe;
	}

    /**
     * Getter order
     * @return {string}
     */
	public get order(): string {
		return this._order;
	}

    /**
     * Getter family
     * @return {string}
     */
	public get family(): string {
		return this._family;
	}

    /**
     * Getter genus
     * @return {string}
     */
	public get genus(): string {
		return this._genus;
	}

    /**
     * Getter species
     * @return {string}
     */
	public get species(): string {
		return this._species;
	}

    /**
     * Getter variety
     * @return {string}
     */
	public get variety(): string {
		return this._variety;
	}

    /**
     * Setter domain
     * @param {string} value
     */
	public set domain(value: string) {
		this._domain = value;
	}

    /**
     * Setter kingdom
     * @param {string} value
     */
	public set kingdom(value: string) {
		this._kingdom = value;
	}

    /**
     * Setter phylum
     * @param {string} value
     */
	public set phylum(value: string) {
		this._phylum = value;
	}

    /**
     * Setter classe
     * @param {string} value
     */
	public set classe(value: string) {
		this._classe = value;
	}

    /**
     * Setter order
     * @param {string} value
     */
	public set order(value: string) {
		this._order = value;
	}

    /**
     * Setter family
     * @param {string} value
     */
	public set family(value: string) {
		this._family = value;
	}

    /**
     * Setter genus
     * @param {string} value
     */
	public set genus(value: string) {
		this._genus = value;
	}

    /**
     * Setter species
     * @param {string} value
     */
	public set species(value: string) {
		this._species = value;
	}

    /**
     * Setter variety
     * @param {string} value
     */
	public set variety(value: string) {
		this._variety = value;
	}

    
	
}