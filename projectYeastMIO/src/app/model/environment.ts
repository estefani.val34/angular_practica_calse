/**
 *class of environment. Related with component environment
 *
 * @export
 * @class Environment
 */
export class Environment{
    private _geography: string;
    private _weather: string;

	/**
     *Creates an instance of Environment.
     * @param {string} [geography]
     * @param {string} [weather]
     * @memberof Environment
     */
    constructor(geography?: string, weather?: string) {
		this._geography = geography;
		this._weather = weather;
	}

    /**
     * Getter geography
     * @return {string}
     */
	public get geography(): string {
		return this._geography;
	}

    /**
     * Getter weather
     * @return {string}
     */
	public get weather(): string {
		return this._weather;
	}

    /**
     * Setter geography
     * @param {string} value
     */
	public set geography(value: string) {
		this._geography = value;
	}

    /**
     * Setter weather
     * @param {string} value
     */
	public set weather(value: string) {
		this._weather = value;
	}
    
}