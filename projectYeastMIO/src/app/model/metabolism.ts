/**
 *class metabolism . Related with metabolism .
 *
 * @export
 * @class Metabolism
 */
export class Metabolism{
    private _aerobic: Boolean;
    private _temperature: number;//ngOnInit aparece unidades temperatura
    private _nutrition: string;


	/**
     *Creates an instance of Metabolism.
     * @param {Boolean} [aerobic]
     * @param {number} [temperature]
     * @param {string} [nutrition]
     * @memberof Metabolism
     */
    constructor(aerobic?: Boolean, temperature?: number, nutrition?: string) {
		this._aerobic = aerobic;
		this._temperature = temperature;
		this._nutrition = nutrition;
	}

    /**
     * Getter aerobic
     * @return {Boolean}
     */
	public get aerobic(): Boolean {
		return this._aerobic;
	}

    /**
     * Getter temperature
     * @return {number}
     */
	public get temperature(): number {
		return this._temperature;
	}

    /**
     * Getter nutrition
     * @return {string}
     */
	public get nutrition(): string {
		return this._nutrition;
	}

    /**
     * Setter aerobic
     * @param {Boolean} value
     */
	public set aerobic(value: Boolean) {
		this._aerobic = value;
	}

    /**
     * Setter temperature
     * @param {number} value
     */
	public set temperature(value: number) {
		this._temperature = value;
	}

    /**
     * Setter nutrition
     * @param {string} value
     */
	public set nutrition(value: string) {
		this._nutrition = value;
	}
	

}