/**
 *class of fermentation. Related with component products. 
 *
 * @export
 * @class Fermentation
 */
export class Fermentation{
    private _id: number;    
    private _type:string; // láctica , alcoholica


	/**
     *Creates an instance of Fermentation.
     * @param {number} id
     * @param {string} type
     * @memberof Fermentation
     */
    constructor(id?: number, type?: string) {
		this._id = id;
		this._type = type;
	}

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter type
     * @return {string}
     */
	public get type(): string {
		return this._type;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter type
     * @param {string} value
     */
	public set type(value: string) {
		this._type = value;
	}
	
}