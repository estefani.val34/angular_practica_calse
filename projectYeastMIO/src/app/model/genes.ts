/**
 *class genes. Related with component genes
 *
 * @export
 * @class Genes
 */
export class Genes{
    private _secuence: string; 
    private _type: string;
    private _protein: boolean;

	/**
     *Creates an instance of Genes.
     * @param {string} [secuence]
     * @param {string} [type]
     * @param {boolean} [protein]
     * @memberof Genes
     */
    constructor(secuence?: string, type?: string, protein?: boolean) {
		this._secuence = secuence;
		this._type = type;
		this._protein = protein;
	}

    /**
     * Getter secuence
     * @return {string}
     */
	public get secuence(): string {
		return this._secuence;
	}

    /**
     * Getter type
     * @return {string}
     */
	public get type(): string {
		return this._type;
	}

    /**
     * Getter protein
     * @return {boolean}
     */
	public get protein(): boolean {
		return this._protein;
	}

    /**
     * Setter secuence
     * @param {string} value
     */
	public set secuence(value: string) {
		this._secuence = value;
	}

    /**
     * Setter type
     * @param {string} value
     */
	public set type(value: string) {
		this._type = value;
	}

    /**
     * Setter protein
     * @param {boolean} value
     */
	public set protein(value: boolean) {
		this._protein = value;
	}

}