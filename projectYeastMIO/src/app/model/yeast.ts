import { Taxonomy } from './taxonomy';

/**
 *classs yeast. Related with component yeast. Call class taxonomy.
 *
 * @export
 * @class Yeast
 */
export class Yeast{
    private _id: string; 
    private _taxonomy: Taxonomy;
    private _classificationDate: string;
    private _temperature:number;


	/**
     *Creates an instance of Yeast.
     * @param {string} [id]
     * @param {Taxonomy} [taxonomy]
     * @param {string} [classificationDate]
     * @param {number} [temperature]
     * @memberof Yeast
     */
    constructor(id?: string, taxonomy?: Taxonomy, classificationDate?: string, temperature?:number) {
		this._id = id;
		this._taxonomy = taxonomy;
        this._classificationDate = classificationDate;
        this._temperature = temperature;
    }
    

    /**
     * Getter temperature
     * @return {number}
     */
	public get temperature(): number {
		return this._temperature;
	}

    /**
     * Setter temperature
     * @param {number} value
     */
	public set temperature(value: number) {
		this._temperature = value;
	}


    /**
     * Getter id
     * @return {number}
     */
	public get id(): string {
		return this._id;
	}

    /**
     * Getter taxonomy
     * @return {Taxonomy}
     */
	public get taxonomy(): Taxonomy {
		return this._taxonomy;
	}

    /**
     * Getter classificationDate
     * @return {string}
     */
	public get classificationDate(): string {
		return this._classificationDate;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: string) {
		this._id = value;
	}

    /**
     * Setter taxonomy
     * @param {Taxonomy} value
     */
	public set taxonomy(value: Taxonomy) {
		this._taxonomy = value;
	}

    /**
     * Setter classificationDate
     * @param {string} value
     */
	public set classificationDate(value: string) {
		this._classificationDate = value;
	}


}

