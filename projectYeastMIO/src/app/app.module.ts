import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';

registerLocaleData(localeES);

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { YeastComponent } from './yeast/yeast.component';
import { EnvironmentComponent } from './environment/environment.component';
import { PatologyComponent } from './patology/patology.component';
import { GenesComponent } from './genes/genes.component';
import { ProductsComponent } from './products/products.component';
import { MetabolismComponent } from './metabolism/metabolism.component';
import { YeastManagementComponent } from './yeast-management/yeast-management.component';

import { InputValidationDirective } from './directives/input-validation.directive';
import { InputValidationNameDirective } from './directives/input-validation-name.directive';

import {CookieService} from 'ngx-cookie-service';

import { NgxPaginationModule } 
  from 'ngx-pagination';


const appRoutes: Routes = [
  { path: 'app-yeast', component: YeastComponent},
  { path: 'app-products', component: ProductsComponent},
  { path: 'app-genes', component: GenesComponent},
  { path: 'app-patology', component: PatologyComponent},
  { path: 'app-metabolism', component: MetabolismComponent},
  { path: 'app-environment', component: EnvironmentComponent},
  { path: 'app-yeast-management', component: YeastManagementComponent},
  { path: '',  redirectTo: '/app-yeast', pathMatch: 'full' },
]

@NgModule({
  declarations: [
    AppComponent,
    YeastComponent,
    EnvironmentComponent,
    PatologyComponent,
    GenesComponent,
    ProductsComponent,
    MetabolismComponent,
    InputValidationDirective,
    InputValidationNameDirective,
    YeastManagementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    RouterModule.forRoot(
      appRoutes, {enableTracing: false}  //True for debugging purposes only
    ),
    NgxPaginationModule
  ],
  providers: [ CookieService,
    {provide: LOCALE_ID,
    useValue: 'es'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
