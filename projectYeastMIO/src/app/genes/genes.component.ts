import { Component, OnInit, ViewChild } from '@angular/core';
import { Genes } from '../model/genes';
import { CookieService } from 'ngx-cookie-service';

/**
 *class related with genes.component.html
 *
 * @export
 * @class GenesComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-genes',
  templateUrl: './genes.component.html',
  styleUrls: ['./genes.component.css']
})
export class GenesComponent implements OnInit {

  @ViewChild('genesEntryForm', null)
  genesEntryForm: HTMLFormElement;


  //properties
  objGenes: Genes;
  cookieObj: any;
  NAtypes: string[] = ['dna', 'rna'];
  Protboolean: boolean[] = [true, false];

  constructor(private cookieService: CookieService) { }

  /**
   *method invoked when loading the page
   *
   * @memberof GenesComponent
   */
  ngOnInit() {
    if (!this.objGenes) {
      this.objGenes = new Genes();
      this.objGenes.type =this.NAtypes[1];
      this.objGenes.protein =this.Protboolean[1];
    }
    this.getCookie();

  }


  /**
   *method invoked when I click botton Submit. Create a cookie.
   *
   * @memberof GenesComponent
   */
  getCookie() {
    if (this.cookieService.check("objGenes")) {
      this.cookieObj = JSON.parse(this.cookieService.get("objGenes"));

      Object.assign(this.objGenes, this.cookieObj);

      this.objGenes.secuence = this.cookieObj._secuence;
      this.objGenes.type = this.cookieObj._type;
      this.objGenes.protein = this.cookieObj._protein;

    }
  }

  /**
   *method invoked when I click botton Submit. Call cookie and show genes object 
   *
   * @memberof GenesComponent
   */
  genesEntry(): void {
    this.cookieService.set("objGenes", JSON.stringify(this.objGenes));
    console.log(this.objGenes);
  }


}
