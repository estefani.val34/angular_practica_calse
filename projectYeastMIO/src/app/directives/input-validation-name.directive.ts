import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appInputValidationName]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: InputValidationNameDirective,
    multi: true
  }]
})
export class InputValidationNameDirective  {

  constructor() { }

}
