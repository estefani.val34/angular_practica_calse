import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatologyComponent } from './patology.component';

describe('PatologyComponent', () => {
  let component: PatologyComponent;
  let fixture: ComponentFixture<PatologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
