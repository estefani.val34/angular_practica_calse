import { Component, OnInit } from '@angular/core';
import { Patology } from '../model/patology';
import { CookieService } from 'ngx-cookie-service';

/**
 *class related with patology.component.html
 *
 * @export
 * @class PatologyComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-patology',
  templateUrl: './patology.component.html',
  styleUrls: ['./patology.component.css']
})
export class PatologyComponent implements OnInit {

  //properties
  objPatology: Patology;
  cookieObj: any;
  constructor(private cookieService: CookieService) { }

  /**
   *method invoked when loading the page
   *
   * @memberof PatologyComponent
   */
  ngOnInit() {
    this.objPatology = new Patology(false, "", false);
    this.getCookie();
  }

  /**
   *method invoked when I click botton Submit. Call cookie and show patology object
   *
   * @memberof PatologyComponent
   */
  getPatology() {
    this.cookieService.set("objPatology", JSON.stringify(this.objPatology));
    console.log(this.objPatology)
  }

  /**
   *method invoked when I click botton Submit. Create a cookie.
   *
   * @memberof PatologyComponent
   */
  getCookie() {
    if (this.cookieService.check("objPatology")) {
      this.cookieObj = JSON.parse(this.cookieService.get("objPatology"));
      //Object.assign(this.objEnvironment, this.cookieObj);
      this.objPatology = new Patology(this.cookieObj._hpatologenic, this.cookieObj._symptoms, this.cookieObj._lethal);
    }
  }
}
