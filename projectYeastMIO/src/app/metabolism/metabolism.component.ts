
import { Component, OnInit, ViewChild } from '@angular/core';
import { Metabolism } from '../model/metabolism';

/**
 *class related with metabolism.component.html
 *
 * @export
 * @class MetabolismComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-metabolism',
  templateUrl: './metabolism.component.html',
  styleUrls: ['./metabolism.component.css']
})
export class MetabolismComponent implements OnInit {
 
  @ViewChild('divCheckBox', null) 
  divCheckBox: HTMLFormElement; 
  //properties
  objMetabolism: Metabolism;
  specialMetabolism: String[] = ["Aerobic"];
  
  constructor() { }

  /**
   *method invoked when loading the page
   *
   * @memberof MetabolismComponent
   */
  ngOnInit() {
    this.objMetabolism = new Metabolism();
    this.objMetabolism.temperature = 10;
    this.objMetabolism.aerobic =false;
    this.fcheckbox();
  }

  /**
   *method invoked when I click botton Submit.
   *
   * @memberof MetabolismComponent
   */
  getMetabolism() {
    console.log(this.objMetabolism);
  }


  /**
   *method taht sum at the temperature when I check box
   *
   * @memberof MetabolismComponent
   */
  fcheckbox():void{
    if(this.objMetabolism.aerobic){
      this.objMetabolism.temperature = this.objMetabolism.temperature+5;
    }else{
      this.objMetabolism.temperature = 10;
    }
  }

}




