import { Component, OnInit, ViewChild, Input }from '@angular/core';
import { DatePipe } from '@angular/common';

import { Yeast } from '../model/yeast';
import { Taxonomy } from '../model/taxonomy';

import { CookieService } from 'ngx-cookie-service';


/**
 *
 *Component related to yeast.component.html
 * @export
 * @class YeastComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-yeast',
  templateUrl: './yeast.component.html',
  styleUrls: ['./yeast.component.css'],
  providers: [DatePipe]
})

export class YeastComponent implements OnInit {


  @ViewChild('reservationEntryForm', null)
  yeastEntryForm: HTMLFormElement;


  @Input() objYeast: Yeast; //need to pagination  of yeast-management.ts
  cookieObj: any;

  constructor(
    private cookieService: CookieService,
    private datePipe: DatePipe) { }

  /**
   *method invoked when loading the page
   *
   * @memberof YeastComponent
   */
  ngOnInit() {
    this.inicializeForm();
    // this.getCookie();
  }

  /**
   *
   *method that inicialize values of form 
   * @memberof YeastComponent
   */
  inicializeForm() {
    if (!this.objYeast) {
      this.objYeast = new Yeast();
      this.objYeast.taxonomy = new Taxonomy();
      this.objYeast.classificationDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    }
  }

 /**
   *cookie for yeast
   *
   * @memberof YeastComponent
   */
  // getCookie() {
  //   if (this.cookieService.check("objYeast")) {
  //     this.cookieObj = JSON.parse(this.cookieService.get("objYeast"));

  //     Object.assign(this.objYeast, this.cookieObj);

  //     this.objYeast.taxonomy = new Taxonomy(this.cookieObj._taxonomy._id, this.cookieObj._taxonomy._domain, this.cookieObj._taxonomy._kingdom,
  //       this.cookieObj._taxonomy._phylum, this.cookieObj._taxonomy._classe,
  //       this.cookieObj._taxonomy._order, this.cookieObj._taxonomy._family, this.cookieObj._taxonomy._genus, this.cookieObj._taxonomy._species,
  //       this.cookieObj._taxonomy._variety)
  //   }
  // }


  /**
   *method invoked when click botton submit
   *
   * @memberof YeastComponent
   */
  yeastEntry(): void {
   // this.cookieService.set("objYeast", JSON.stringify(this.objYeast));
    console.log(this.objYeast);
  }
  
}
