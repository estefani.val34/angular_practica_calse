import {ReservationTime} from "./ReservationTime"
import {TablePreference} from "./TablePreference"
import {SpecialRequests} from "./SpecialRequests"

export class Reservation{
    // Properties
    private _id: number;
    private _name: string;
    private _surname: string;
    private _email: string;
    private _phone: string;
    private _reservationDate: string;
    private _reservationTime: ReservationTime;
    private _tablePreference: TablePreference;
    private _specialRequests: SpecialRequests[];
    private _totalPrice:number;


	constructor(id?: number, name?: string, surname?: string, email?: string, phone?: string, reservationDate?: string, reservationTime?: ReservationTime, tablePreference?: TablePreference, specialRequests?: SpecialRequests[], totalPrice?: number) {
		this._id = id;
		this._name = name;
		this._surname = surname;
		this._email = email;
		this._phone = phone;
		this._reservationDate = reservationDate;
		this._reservationTime = reservationTime;
		this._tablePreference = tablePreference;
		this._specialRequests = specialRequests;
		this._totalPrice = totalPrice;
	}


    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter name
     * @return {string}
     */
	public get name(): string {
		return this._name;
	}

    /**
     * Getter surname
     * @return {string}
     */
	public get surname(): string {
		return this._surname;
	}

    /**
     * Getter email
     * @return {string}
     */
	public get email(): string {
		return this._email;
	}

    /**
     * Getter phone
     * @return {string}
     */
	public get phone(): string {
		return this._phone;
	}

    /**
     * Getter reservationDate
     * @return {Date}
     */
	public get reservationDate(): string {
		return this._reservationDate;
	}

    /**
     * Getter reservationTime
     * @return {ReservationTime}
     */
	public get reservationTime(): ReservationTime {
		return this._reservationTime;
	}

    /**
     * Getter tablePreference
     * @return {TablePreference}
     */
	public get tablePreference(): TablePreference {
		return this._tablePreference;
	}

    /**
     * Getter specialRequests
     * @return {SpecialRequests[]}
     */
	public get specialRequests(): SpecialRequests[] {
		return this._specialRequests;
	}

    /**
     * Getter totalPrice
     * @return {number}
     */
	public get totalPrice(): number {
		return this._totalPrice;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter name
     * @param {string} value
     */
	public set name(value: string) {
		this._name = value;
	}

    /**
     * Setter surname
     * @param {string} value
     */
	public set surname(value: string) {
		this._surname = value;
	}

    /**
     * Setter email
     * @param {string} value
     */
	public set email(value: string) {
		this._email = value;
	}

    /**
     * Setter phone
     * @param {string} value
     */
	public set phone(value: string) {
		this._phone = value;
	}

    /**
     * Setter reservationDate
     * @param {Date} value
     */
	public set reservationDate(value: string) {
		this._reservationDate = value;
	}

    /**
     * Setter reservationTime
     * @param {ReservationTime} value
     */
	public set reservationTime(value: ReservationTime) {
		this._reservationTime = value;
	}

    /**
     * Setter tablePreference
     * @param {TablePreference} value
     */
	public set tablePreference(value: TablePreference) {
		this._tablePreference = value;
	}

    /**
     * Setter specialRequests
     * @param {SpecialRequests[]} value
     */
	public set specialRequests(value: SpecialRequests[]) {
		this._specialRequests = value;
	}

    /**
     * Setter totalPrice
     * @param {number} value
     */
	public set totalPrice(value: number) {
		this._totalPrice = value;
	}


}