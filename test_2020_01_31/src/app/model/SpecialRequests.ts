export class SpecialRequests{
    private id: number;
    private request: string;
    private price: number;


	constructor($id: number, $request: string, $price: number) {
		this.id = $id;
		this.request = $request;
		this.price = $price;
	}


    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $request
     * @return {string}
     */
	public get $request(): string {
		return this.request;
	}

    /**
     * Getter $price
     * @return {number}
     */
	public get $price(): number {
		return this.price;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $request
     * @param {string} value
     */
	public set $request(value: string) {
		this.request = value;
	}

    /**
     * Setter $price
     * @param {number} value
     */
	public set $price(value: number) {
		this.price = value;
	}

}