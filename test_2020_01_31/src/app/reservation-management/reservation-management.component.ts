import { Component, OnInit } from '@angular/core';
import { Reservation } from '../model/Reservation';
import { ReservationService } from '../services/reservation.service';

@Component({
  selector: 'app-reservation-management',
  templateUrl: './reservation-management.component.html',
  styleUrls: ['./reservation-management.component.css']
})
export class ReservationManagementComponent implements OnInit {
  //Filter properties
  priceFilter: number = 0;
  nameFilter: string = "";
  
  //Pagination properties
  currentPage: number;
  itemsPerPage: number;

  reservations: Reservation[]=[];
  reservationsFiltered: Reservation[]=[];

  reservationSelected: Reservation;

  constructor(private reservationService: ReservationService) { 
  }

  ngOnInit() {
    this.reservations = this.reservationService.
      generateReservationsRandom();
    this.reservationsFiltered = this.reservations;

    this.priceFilter=500;
    this.itemsPerPage=10;
    this.currentPage=1;
  }

  filter(){
    this.reservationsFiltered = this.reservations.filter(reservation => {
        let priceValid: boolean = false;
        let nameValid: boolean = false;

        priceValid=(reservation.totalPrice<=this.priceFilter);

        // IndexOf: Returns the position of the 
        // first occurrence of a substring.
        // Otherwise returns -1
        if(this.nameFilter && this.nameFilter!=""){
          if(reservation.name.toLowerCase().indexOf (this.nameFilter.toLowerCase()) !=-1){
            nameValid=true;
          }
        }else{
          nameValid=true;
        }
        
        return nameValid && priceValid;

      })
  }

  onClick(rv: Reservation){
    this.reservationSelected = rv;
  }

  removeRes(rv: Reservation){
    console.log(this.reservations.indexOf(rv));
    console.log(this.reservationsFiltered.indexOf(rv));
    
    this.reservations.splice(this.reservations.indexOf(rv),1);
    this.reservationsFiltered.splice(this.reservationsFiltered.indexOf(rv),1);
  }
}
