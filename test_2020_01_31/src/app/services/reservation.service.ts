import { Injectable } from '@angular/core';
import { ReservationTime } from '../model/ReservationTime';
import { TablePreference } from '../model/TablePreference';
import { SpecialRequests } from '../model/SpecialRequests';
import { Reservation } from '../model/Reservation';
import { StringMap } from '@angular/compiler/src/compiler_facade_interface';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor() { }

  createReservationTimes(): ReservationTime[]{
    let reservationTimes: ReservationTime[] = [];
    //A temporary array of ReservationTimes is builded due to the lack of database
    let reservationTimesAux: string[] = ["12:00", "13:00", "14:00", "15:00"];

    for (let i: number = 0; i < reservationTimesAux.length; i++) {
      reservationTimes.push(new ReservationTime(i, reservationTimesAux[i]));
    }

    return reservationTimes;
  }

  createReservationPreferences(): TablePreference[] {
    let tablePreferences: TablePreference[] = [];
    let tablePreferencesAux: string[] =
      ["Next to the window", "Next to the door",
        "Private room"];
    let tablePreference: TablePreference; //Temp variable

    for (let i: number = 0; i < tablePreferencesAux.length; i++) {
      tablePreference = new TablePreference(i,
        tablePreferencesAux[i], i * 2 + 3);
        tablePreferences.push(tablePreference);
    }
    return tablePreferences;
  }

  createSpecialRequests(): SpecialRequests[]{
    let specialRequests: SpecialRequests[] = [];
    let specialRequestAux: string[] =
     ["Vegetarian menu", "Lactose intolerance",
     "Celiac"];
    let specialRequest: SpecialRequests; //Temp variable

    for(let i: number = 0; i< specialRequestAux.length; i++  ){
      specialRequest = new SpecialRequests(i,
        specialRequestAux[i], i*3+1);
      specialRequests.push(specialRequest);
    }
    return specialRequests;
  }

  generateReservationsRandom(): Reservation[]{
    let reservations: Reservation[]=[];

    let randomName: string;
    let randomReservPrice: number;
    let randomTablePreference: number;
    let randomReservationTime: number;
    let today: Date = new Date(); 

    let reservation: Reservation;

    for(let i=0; i<369; i++){
      randomReservPrice = Math.floor(
        Math.random()*501);
      randomTablePreference = Math.floor(
        Math.random()*3);
      randomReservationTime = Math.floor(
          Math.random()*4);
        
      if(i%5==0){
        randomName = "pepeeee";
      }else{
        randomName = "Nameee"+i;
      }

      reservation = new Reservation(i,
        randomName, "Surname"+i, "email"+i+"@gmail.com",
        "666666666", 
        today.getFullYear() + "/"+ 
        (today.getMonth()+1)
        + "/" + today.getDate(),
        this.createReservationTimes()[randomReservationTime],
        this.createReservationPreferences()[randomTablePreference],
        [],randomReservPrice);
 
      reservations.push(reservation);
     }
    return reservations;
  }

}
